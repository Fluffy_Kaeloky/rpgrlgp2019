﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AIInputSystem))]
public class AIController : MonoBehaviour
{
    public float range = 3.0f;
    public float sprintRange = 8.0f;
    public Transform target = null;

    private AIInputSystem inputSystem = null;

    private void Start()
    {
        inputSystem = GetComponent<AIInputSystem>();
    }

    private void FixedUpdate()
    {
        inputSystem.SetLookDir(target.position - transform.position);
        inputSystem.SetForward(Vector3.Distance(target.position, transform.position) > range ? 1.0f : 0.0f);
        inputSystem.SetStrafe(0.0f);
    }
}
