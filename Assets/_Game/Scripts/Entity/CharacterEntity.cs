﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof (Damageable))]
public class CharacterEntity : MonoBehaviour
{

    [SerializeField] private string entityName = "Jack";
    bool isDestroyed = false;

	public Caract Carac { get { return carac; } private set { carac = value; } }
    Caract carac = new Caract();
	public Arsenal Arsenal { get { return arsenal; } private set { arsenal = value; } }
    Arsenal arsenal = new Arsenal();
    Damageable damageable;

    void Start ()
    {
        damageable.maxHP = carac.CalcMaxHp();
        damageable.hp = GetComponent<Damageable>().maxHP;

        damageable.onDamageTaken.AddListener(OnDamageTakenCallback);
        damageable.onDeath.AddListener(OnDeathCallback);
	}

    private void OnDamageTakenCallback(OnDamageTakenArgs args)
    {
        Debug.Log("Resist ! : damage / 2");

        args.damageAmount.Value /= 2.0f;

        GetComponent<Renderer>().material.color = Color.yellow;
    }

    private void OnDeathCallback(OnDeathArgs args)
    {
        //print message like "World Killed you!!" with args.source.getName()
        GetComponent<Renderer>().material.color = Color.red;
        //Destroy(gameObject);
    }

    void DestroyEntity()
    {
        //possibility to add anim before
        Destroy(gameObject);
    }
}
