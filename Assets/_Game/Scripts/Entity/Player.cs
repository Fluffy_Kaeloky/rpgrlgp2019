﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterEntity))]
public class Player : MonoBehaviour
{
	public CharacterEntity CharEntity { get { return charEntity; } private set { charEntity = value; } }
	private CharacterEntity charEntity;

	[SerializeField] private Inventory inventory;

	public uint Level { get { return level; } set { level = value; } }
	private uint level;

	void Start ()
	{
		charEntity = GetComponent<CharacterEntity>();
	}
	
	void Update ()
	{
	
	}
}
