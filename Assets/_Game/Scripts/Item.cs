﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Usable))]
public class Item : MonoBehaviour
{
	public enum ItemType : int
	{
		Equipment = 0,
		Weapon,
		Key,
		Consommable
	}

	public ItemType EnumItemType { get { return enumItemType; } set { enumItemType = value; } }
	private ItemType enumItemType;

	public enum EquipmentEmplacement : int
	{
		Head = 0,
		Chest,
		Feet,
		Shield
	}

	public EquipmentEmplacement EnumEquipmentEmplacement { get { return enumEquipmentEmplacement; } set { enumEquipmentEmplacement = value; } }
	private EquipmentEmplacement enumEquipmentEmplacement;

	public uint Weight { get { return weight; } set { weight = value; } }
	private uint weight;

	public string ItemName { get { return itemName; } set { itemName = value; } }
	private string itemName;

	public uint ItemValue { get { return itemValue; } set { itemValue = value; } }
	private uint itemValue;

	public uint ParticularStat { get { return particularStat; } set { particularStat = value; } }
	private uint particularStat;

	public string Description { get { return description; } set { description = value; } }
	private string description;

    private bool isUsed = false;

	void Start ()
	{
        GetComponent<Usable>().onUsable.AddListener(OnUseableCallback);
    }
	
    private void OnUseableCallback(OnUsableArg arg)
    {
        if (!isUsed)
        {
            Debug.Log("Open Chest");
            isUsed = true;
        }
    }

	public Item(ItemType _enumItemType, string _name, uint _weight, uint _itemValue, uint armorOrDamages)
	{
		enumItemType = _enumItemType;
		itemName = _name;
		weight = _weight;
		itemValue = _itemValue;
		particularStat = armorOrDamages;
	}
}

public class ItemComparer : System.Collections.Generic.IEqualityComparer<Item>
{
	public bool Equals(Item item1, Item item2)
	{
		if (item1.EnumItemType == item2.EnumItemType && item1.Weight == item2.Weight && item1.ItemName == item2.ItemName && item1.ParticularStat == item2.ParticularStat && item1.Description == item2.Description)
			return true;
		return false;
	}

	public int GetHashCode(Item item)
	{
		string code = item.EnumItemType.ToString() + "|" + item.Weight.ToString() + "|" + item.ItemName.ToString() + "|" + item.ParticularStat.ToString() + "|" + item.Description;
		return code.GetHashCode();
	}
}
