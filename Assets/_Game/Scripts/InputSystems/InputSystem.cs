﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

abstract class InputSystem : MonoBehaviour
{
    public abstract float GetForward();
    public abstract float GetStrafe();
    public abstract Vector3 GetLookDir();
    public abstract bool GetSprint();

    public UnityEvent onPrincipalFire;
    public UnityEvent onPrincipalFireUp;

    public UnityEvent onSecondaryFire;
    public UnityEvent onSecondaryFireUp;
}
