﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemCharacteristics : MonoBehaviour
{
	[SerializeField] private Text nameLabel;
	[SerializeField] private Text particularLabel;
	[SerializeField] private Text weightLabel;
	[SerializeField] private Text valueLabel;

	void Start ()
	{
		Hide();
	}

	public void Display(Item item, uint nb_item)
	{
		gameObject.SetActive(true);

		nameLabel.text = nb_item > 1 ? item.ItemName.ToUpper() + " (" + nb_item + ")" : item.ItemName.ToUpper();

		if (item.ParticularStat != 0)
		{
			if (item.EnumItemType == Item.ItemType.Weapon)
				particularLabel.text = "Damages : " + item.ParticularStat;
			else
				particularLabel.text = "Armor : " + item.ParticularStat;
		}
		
		weightLabel.text = "Weight : " + item.Weight.ToString();
		valueLabel.text = "Value : " + item.ItemValue.ToString();
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
