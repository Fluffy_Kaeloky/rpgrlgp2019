﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryLowerBar : MonoBehaviour
{
	[SerializeField] private Text damagesLabel;
	[SerializeField] private Text modifierLabel;
	[SerializeField] private Text weightLabel;
	[SerializeField] private Text goldLabel;

	[SerializeField] private Button destroyButton;
	[SerializeField] private Button favoriteButton;

	public void ChangeParticularStat(string armorOrDamages, uint value)
	{
		if (value != 0)
			damagesLabel.text = armorOrDamages + " : " + value;
		else
			damagesLabel.text = "";
	}

	public void ChangeModifierLabel(int modifier)
	{
		if (modifier == 0)
			return;

		modifierLabel.color = modifier > 0 ? Color.green : Color.red;

		modifierLabel.text = "( " + modifier + " )";
	}

	public void ChangeWeightLabel(uint curWeight, uint maxWeight)
	{
		weightLabel.text = "Weight : " + curWeight + " / " + maxWeight;
	}

	public void ChangeGoldLabel(uint gold)
	{
		goldLabel.text = "Gold : " + gold;
	}

	public void ShowButtons()
	{
		destroyButton.gameObject.SetActive(true);
		//favoriteButton.gameObject.SetActive(true);
	}

	public void HideButtons()
	{
		destroyButton.gameObject.SetActive(false);
		favoriteButton.gameObject.SetActive(false);
	}
}
