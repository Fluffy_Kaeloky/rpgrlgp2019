﻿using UnityEngine;
using System.Collections;

public class Arsenal : MonoBehaviour {

    public Item headArmor;
    public Item chestArmor;
    public Item feetsArmor;
    public Item weapon;
    public Item shield;
}
